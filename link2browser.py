# !/usr/bin/python3
from tkinter import filedialog
from tkinter import *
import os
import json
import sys
from pathlib import Path

root = Tk()

# root.geometry('1760x700')

home_dir = Path.home()
home_dir = str(home_dir)

os_name = os.name
print(os_name)
script_name = os.path.basename(__file__)
app_data = {
    'app_name': 'link2browser',
    'version': '0.1.0',
    'author': 'andy gaal'
}

curr_ver = 1.0
curr_dir = os.getcwd()

path_info = sys.argv
print('*path_info_0:' + path_info[0])
print('home_dir:'+home_dir);
if os_name == 'nt':
    file_path = path_info[0].replace('/', '\\')
else:
    file_path = path_info[0]
folder_name = file_path.rsplit('/', 1)

if os_name == 'nt':
    process_path = file_path.rsplit('\\', 1)
    slsh = '\\'
else:
    process_path = file_path.rsplit('/', 1)
    slsh = '/'



script_path = process_path[0]
script_dir = os.path.dirname(script_path)


print('script name:' + script_name)
print('sript path:' + script_path)

bname = ['Firedragon', 'Librewolf', 'Basilisk', 'Vivaldi', 'Palemoon', 'Firefox', 'Firefox-develop', 'Tor-Browser',
         'Falkon',
         'Konqueror', 'Midori', 'Chromium', 'Chrome', 'Edge']
bpath = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
entry = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
chkbtn = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
savebtn= ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
icobtn = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
frame = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
icon = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']
button = ['', '', '', '', '', '', '', '', '', '', '', '', '', '']

bcount = len(bname)

print('count browsers: ' + str(bcount))
keys = ['browser_name', 'browser_path']

init_data = {}
dict1 = {}


def chkfilepath(file):
    if os.path.isfile(file):
        fchk = True
    else:
        fchk = False
    return fchk


root.title = 'py link route to browser ' + str(curr_ver)
root.resizable(True, True)

root.config(bg='#002446')

addressbar = Entry(root, width=170)
addressbar.config(fg='darkblue', bg='white', font='Helvetica 14 bold', justify='left')
# addressbar.grid(row=0, column=0, columnspan=4, padx=0, pady=10)

addressbar.delete(0, END)

cp = ''
p = 1
cmd_param = sys.argv
param_len = len(cmd_param)

if len(cmd_param) > 1:
    for q in range(1, param_len):
        cp = cp + cmd_param[q]
else:
    cp = 'laugi.com'

addressbar.insert(END, str(cp))


def start_browser(bv):
    link = addressbar.get()
    print('browser:' + bv)
    if os_name == 'nt':
        cmd_post = cmdpost = bv + ' ' + link
    else:
        cmdpost = bv + ' ' + link
    print(cmdpost)
    os.system(cmdpost)


def read_bfile():
    with open(home_dir + '/.link2browser/browsers.json', 'r') as openfile:
        dict1 = json.load(openfile)
        # print('reading file: ' + str(dict1))
        cnt = 0
        for cnt in range(bcount):
            bname[cnt] = dict1['browser_name'][cnt]
            # print('>' + bname[cnt])
            bpath[cnt] = dict1['browser_path'][cnt]
            # print('>' + bpath[cnt])
            # print(dict1['browser_path'][2])
            cnt = cnt + 1


def select_browser(bi):
    filename = filedialog.askopenfilename()
    if (os_name == 'nt'):
        filename = '"' + filename + '"'
    entry[bi].delete(0, END)
    entry[bi].insert(END, str(filename))

def update_icon(bi):
    print( str(bi) + 'browser icon')

def update_browser(bi, bp):
    bpath[bi] = entry[bi].get()
    pathchk = bpath[bi].replace('"', '')
    if chkfilepath(pathchk):
        chkbtn[bi].config(bg='green')
    else:
        chkbtn[bi].config(bg='red')
    upd_data = {'browser_name': [], 'browser_path': []}

    for c in range(bcount):
        upd_data['browser_name'].append(bname[c])
        upd_data['browser_path'].append(bpath[c])
    with open(home_dir + '/.link2browser/browsers.json', 'w') as file_target:
        json.dump(upd_data, file_target)


def init_browser_list():
    browser_list = bname
    init_dict = {'browser_name': [], 'browser_path': []}
    for browser in browser_list:
        init_dict['browser_name'].append(browser)
        init_dict['browser_path'].append(browser.lower())
        file_content = 'browsers = ' + str(init_dict)
        with open(home_dir + '/.link2browser/browsers.json', 'w') as file_target:
            json.dump(init_dict, file_target)


if chkfilepath(home_dir + '/.link2browser/browsers.json'):
    read_bfile()
else:
    init_browser_list()

cl = -1
rw = 3
for bn in range(0, bcount):
    cl = cl + 1
    if cl >= 4:
        cl = 0
        rw = rw + 1

    frame[bn] = Frame(root, width=200, height=200, bg='#316898')
    frame[bn].grid(row=rw, column=cl, padx=8, pady=8)
    icon[bn] = PhotoImage(file='img/' + bname[bn].lower() + '.png')

    # brewser path selector
    chkbtn[bn] = Button(frame[bn], width=4, height=1, bg='lightgrey', text='..',
                        command=lambda bn=bn: select_browser(bn))
    chkbtn[bn].place(x=10,y=10) 

    # icon select path
    icobtn[bn] = Button(frame[bn], width=4, height=1, bg='lightgrey', text='icon',
                        command=lambda bn=bn: update_icon(bn))
    icobtn[bn].place(x=120,y=10)   

 
    # ENTRY
    entry[bn] = Entry(frame[bn], width=30)
    entry[bn].place(x=10,y=50)
    entry[bn].insert(0, bpath[bn])
    pathchk = bpath[bn]
    if chkfilepath(pathchk):
        btnbg = 'green'
    else:
        btnbg = 'red'

   

    # start browser button
    button[bn] = Button(frame[bn], width=90, height=90, image=icon[bn], command=lambda bn=bn: start_browser(bpath[bn]))
    button[bn].place(x=10,y=80)
       

    savebtn[bn] = Button(frame[bn], width=5, height=4, bg=btnbg, text='save',
                        command=lambda bn=bn: update_browser(bn, bpath[bn]))
    savebtn[bn].place(x=120,y=85)

root.mainloop()
