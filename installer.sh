#!/bin/bash

sudo rm /usr/bin/link2browser
sudo rm /usr/share/applications/link2browser.desktop
sudo rm /home/andy/.local/share/applications/link2browser.desktop
sudo rm /usr/share/applications/ycom1-link2browser.desktop
sudo rm /home/andy/.local/share/ycom1-applications/link2browser.desktop

echo "#!/bin/bash" | tee $PWD/link2browser
echo "cd $PWD" | tee -a $PWD/link2browser

echo 'python3 link2browser.py $1' | tee -a $PWD/link2browser


sudo chmod 775 $PWD/link2browser
sudo chmod a+x $PWD/link2browser


echo "[Desktop Entry]"             | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Encoding=UTF-8"              | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Version=1.0"                 | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Type=Application"            | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Name=link2browser"           | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Terminal=false"              | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Comment=lets you choose
      which browser you open
      a link with"                 | tee -a /home/$USER/.local/share/applications/link2browser.desktop

echo "Icon=$PWD/link2browser.png"  | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "MimeType=x-scheme-handler/http;x-scheme-handler/https;text/html;application/xhtml+xml;" | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Keywords=Links; Browser"     | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Categories=Network"          | tee -a /home/$USER/.local/share/applications/link2browser.desktop
echo "Exec=link2browser %u"        | tee -a /home/$USER/.local/share/applications/link2browser.desktop


sudo chmod 775 /home/$USER/.local/share/applications/link2browser.desktop
sudo chmod a+x /home/$USER/.local/share/applications/link2browser.desktop


osrelease=$(sudo cat /etc/*-release)

echo $osrelease

distros=("Arch" "OpenSuse" "Debian" "Ubuntu" "Fedora")


for key in "${!distros[@]}"
do
  shopt -s nocasematch
  if [[ $osrelease == *"${distros[$key]}"* ]]; then
    myos="${distros[$key]}"
    myky="$key"
    echo "key: $key"
    echo "myos: $myos"
  fi
done
echo "installer detected ${myos}, please enter password ..."

case $myky in
0)
  sudo pacman -S tk;;
1)
  sudo zypper in python-tk;;
2)
  sudo apt install python3-tk;;
3)
  sudo apt install python3-tk;;
4)
  sudo dnf install python3-tkinter;;
esac

mkdir -p /home/$USER/.link2browser
cp browsers.json /home/$USER/.link2browser

# add app directory

echo export 'PATH=$PATH:/home/andy/Apps/link2browser' | tee -a /home/$USER/.bashrc

#reload environment 

source /home/$USER/.bashrc

echo $PATH

gnome xdg-settings set default-web-browser link2browser.desktop





