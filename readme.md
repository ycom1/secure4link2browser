# Link2Browser



Link2Browser is a python app, by integrating it into your system as default browser, enables you to

open links you click on with the browser of your choice.

This allows for browser separation which is good for privacy reasons, and also lets you have a more

structured workflow on your desktop.



## Installation: 

just unpack the tar.gz intoo you /home/Apps directory, or into a folder of your choice. Inside this unpacked

folder is the installer.sh, which you can execute in the terminal. The installer creates a config directory named

**.link2browser** in your home ddirectory.

After that you need to register link2browser als yyour default browser. Do not forget to add **%U** as command

-line argument in your applications starter menu.

From now on, if you click on a link in your email messenger or other apps, link2browser starts with the 

given link and lets you choose the browser to open this link with. 

Please save each path each  of your browsers at first start, your settings can be changed anytime later

as link2browser started.

### License: MIT

